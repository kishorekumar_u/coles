'use strict';

module.exports = function(grunt) {

	grunt.initConfig({
		// General package information to be used in file banner directives.
		pkg: grunt.file.readJSON('package.json'),


		// Compile LESS
		less: {
			development: {
				options: {
					paths: ["src/libs"]
				},
				files: {
					"../App_themes/FrontEnd/css/style.css": "src/style.less"
				}
			}
		},

		// Watches the following file paths. If a change occurs it will run the requested tasks.
		watch: {
			options: {
				interrupt: true
			},
			css: {
				files: ['src/*.less'],
				tasks: ['less:development']
			}
		}
	});

	// Loading plugins.
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['less']);
	//grunt.registerTask('default', ['less']);
};